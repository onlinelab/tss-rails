class RenameApptableToConsultRequest < ActiveRecord::Migration
  def change
    rename_table :apptables, :consult_requests
  end
end
