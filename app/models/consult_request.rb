# == Schema Information
#
# Table name: consult_requests
#
#  id          :integer          not null, primary key
#  name        :string
#  contact     :string
#  place       :text
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ConsultRequest < ActiveRecord::Base
	validates :name, :contact, :place, :description, presence: true
end
