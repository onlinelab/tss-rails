module ApplicationHelper
  def set_title(page_title)
    content_for(:title) { page_title }
  end
    
  def set_body_class(page_body)
    content_for(:body_class) { page_body }
  end

  def image_for_light_box(image)
    link_to image_tag(image, class: 'example-image'), image_url(image), class: 'example-image-link', data: { lightbox: 'example-set1' }
  end

  def images_list_for_photo_object(range)
    range.collect do |indx|
      content_tag :div, class: 'col-xs-4 tech_bl_new' do
        image_for_light_box("img_object_#{indx}.jpg")
      end
    end.join.html_safe
  end

end
