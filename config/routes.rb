Rails.application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'page/contact'
  post 'page/contact' => 'page#send_feedback', as: :send_feedback
  post 'page/online_applic' => 'page#send_online_app', as: :send_online_app
  get 'page/help_transport'
  get 'page/object'
  get 'page/online_applic'
  get 'page/photo_object_cotel'
  get 'page/photo_object_optica'
  get 'page/photo_object_sochi'
  get 'page/photo_object_transformer'
  get 'page/photo_object_apteki'
  get 'page/photo_object_svinkomplex'
  get 'page/photo_object_hlebzavod'
  get 'page/photo_object_agrotechnopark'
  get 'page/photo_object_svobodnaya'
  get 'page/reference'
  get 'page/sro'
  get 'page/technolog_park'
  get 'page/create_app'
  get 'page/create_message'

  root 'page#index'
end
