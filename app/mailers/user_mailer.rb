class UserMailer < ActionMailer::Base
	default from: "no-reply@tss68.ru",
			to: "feedback@tss68.ru"
	layout 'mailer'

 	def feedback(name,contact,message)
		@name = name
		@contact = contact
		@message = message
		mail(subject: 'Обратная связь')
	end

	def online_app(name,contact,place,description)
		@name = name
		@contact = contact
		@place = place
		@description = description
		mail(subject: 'Онлайн заявка')
	end
end
