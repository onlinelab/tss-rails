class CreateApptables < ActiveRecord::Migration
  def change
    create_table :apptables do |t|
      t.string :name
      t.string :contact
      t.text :place
      t.text :description

      t.timestamps
    end
  end
end
