# == Schema Information
#
# Table name: buildings
#
#  id         :integer          not null, primary key
#  name       :string
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Building < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  has_many :photos, dependent: :destroy

  validates :name, :image, presence: true
end
