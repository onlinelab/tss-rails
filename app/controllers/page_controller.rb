class PageController < ApplicationController
  def send_feedback
    UserMailer.feedback(params[:name], params[:contact],params[:message]).deliver_now
    redirect_to page_create_message_path
  end

  def send_online_app
    @consult_request = ConsultRequest.new(consult_request_params)
    respond_to do |format|
    	if @consult_request.save
    		format.html {redirect_to page_create_app_url}
        UserMailer.online_app(params[:name], params[:contact], params[:place], params[:description]).deliver_now
    	else
        format.html {redirect_to page_online_applic_url, notice: 'Не все поля были заполнены!'}
      end
    end
  end

  private
  def consult_request_params
  	params.permit(:name, :contact, :place, :description)
  end
end
