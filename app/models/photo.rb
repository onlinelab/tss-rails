# == Schema Information
#
# Table name: photos
#
#  id         :integer          not null, primary key
#  image      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  special    :string
#  img        :string
#

class Photo < ActiveRecord::Base
	belongs_to :building
	validates :image, :special, presence: true

	mount_uploader :image, ImgUploader
end
